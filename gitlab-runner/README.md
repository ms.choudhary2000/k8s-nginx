# How to setup gitlab runner on kubernetes


### Pre-requisites

- kubernetes cluster up and running
- helm is installed

### Steps

- Add gitlab helm repo

```
helm repo add gitlab https://charts.gitlab.io/
```

- Install

```
helm install gitlab-runner gitlab/gitlab-runner --namespace kube-system -f values.yaml
```
